# Sucrees Desserts v2

## Intro
[Sucrées Desserts](http://sucreesdesserts.com/#/home) is a dessert and pastry store.

This project will: 

* migrate current prototype website from AngularJS (using Typescript) to Angular 4/5
* continue to utilise MongoDB for product storage
* include a suite of unit tests 
* improve website design